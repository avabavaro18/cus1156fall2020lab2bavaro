package cus1156.catlab2;

import java.util.ArrayList;

/**
 * This class represents a group of cats. The cats are created when the CatManager is created
 * although more cats can be added later.
 * 
 */
public class CatManager
{
	private static ArrayList<Cat>  myCats = new ArrayList<Cat>();

	public CatManager()
	{
		Cat cat = new Cat("Fifi", "black");
		myCats.add(cat);
		cat = new Cat("Fluffy", "spotted");
		myCats.add(cat);
		cat = new Cat("Josephine", "tabby");
		myCats.add(cat);
		cat = new Cat("Biff", "tabby");
		myCats.add(cat);
		cat = new Cat("Bumpkin", "white");
		myCats.add(cat);
		cat = new Cat("Spot", "spotted");
		myCats.add(cat);
		cat = new Cat("Lulu", "tabby");
		myCats.add(cat);
                System.out.println(myCats);
                System.out.println();
	}
         
	public static void addCats(Cat aCat)
	{
	    myCats.add(aCat);
            System.out.println("New ArrayList:\n" + myCats);
            System.out.println();
	}

	public static Cat findThisCat(String name)
        {
            Cat a = null;
            int length = myCats.size();
            for (int i = 0; i < length; i++) {
                if (myCats.get(i).getName().contains(name)) {
                    a = myCats.get(i);
                } 
            }
            return a;
	}

	public int countColors(String color)
	{           
            int counter = 0;
            int length = myCats.size();
            for (int i = 0; i < length; i++) {
                if (myCats.get(i).getColor().contains(color)) {
                    counter++;
                } 
            }
            return counter;
	}
}
